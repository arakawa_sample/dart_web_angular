
import 'package:sample_dart_web_angular/src/ayu/ayu_barrel.dart';

enum Situation {
    initial,
    walkForward,
    walkLeft,
    walkRight,
    walkBackward,
}

class Model {

    static final backScreen = BackScreenConfiguration(
        ChecksStatus(
            ChecksStatusBackgroundColor.black(),
            ChecksStatusChecksColor.purple(),
            ChecksStatusSize(25),
            CssBackgroundStatusSituation.initial(),
        ),
    );

    static const px = 32 * 2;
    static final viewport = ViewportConfiguration(
        ViewportStatus(
            ViewportStatusWidth(1400),
            ViewportStatusHeight(800),
            ViewportStatusInsideWidth(px*34),
            ViewportStatusInsideHeight(px*18),
        ),
        FocusStatus(
            FocusStatusWidth(400),
            FocusStatusHeight(300),
            FocusStatusXAxis(100),
            FocusStatusYAxis(100),
        ),
        null,
        CssBackgroundStatusList([
            ChecksStatus(
                ChecksStatusBackgroundColor.black(),
                ChecksStatusChecksColor.green(),
                ChecksStatusSize(10),
                CssBackgroundStatusSituation.initial(),
            ),
        ]),
        ImageBackgroundStatusList([
            ImageBackgroundStatus.pxSize(
                ImageBackgroundStatusSrc('map_sample_p32_w34_h18_1.png'),
                ImageBackgroundStatusWidth(px*34),
                ImageBackgroundStatusHeight(px*18),
                ImageBackgroundStatusXAxisRepeat.off(),
                ImageBackgroundStatusYAxisRepeat.off(),
                ImageBackgroundStatusSituation.initial(),
            ),
        ]),
    );


}