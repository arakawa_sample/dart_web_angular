
import 'dart:async';
import 'dart:html';
import 'package:angular/angular.dart';

/// 20201122
@Component(
    selector: 'sample',
    template: '<h1>sample</h1>',
)
class ResizeSample implements OnInit {

    static const _handred = 100;

    Timer? resizeTimer;

    @override
    void ngOnInit() {
        window.onResize.listen((onData){
            if(resizeTimer != null) resizeTimer!.cancel();
            resizeTimer = Timer(Duration(milliseconds: _handred), (){
                print('do resize');
            });
        });
    }

}
