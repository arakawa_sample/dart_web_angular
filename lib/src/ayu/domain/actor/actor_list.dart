
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'actor.dart';

class ActorList
    extends
        ListComputation<
            Actor
            ,ActorList
            ,SampleDartWebAngularModule
        >
{

    ActorList(this.values);

    @override
    final Iterable<Actor> values;

    @override
    ActorList internalFactory(Iterable<Actor> models) => ActorList(models);

}