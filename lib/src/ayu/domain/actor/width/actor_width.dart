
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/between_x_and_y.dart';
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class ActorWidth
    extends
        NumStateModel
    with
        BetweenXAndY<
            num
            ,ActorWidth
            ,SampleDartWebAngularModule
        >
        ,Between0And10000<
            num
            ,ActorWidth
            ,SampleDartWebAngularModule
        >
        ,NumStateModelGetPx<
            num
        >
        ,BetweenXAndYOperator<
            num
            ,ActorWidth
            ,SampleDartWebAngularModule
        >
{

    /// 0 から 10000 までの数（num 型）
    /// 単位は px.    
    ActorWidth(this.value) {
        validationBetweenXAndY();
    }

    @override
    final num value;

    @override
    ActorWidth internalFactory(num value) => ActorWidth(value);

}