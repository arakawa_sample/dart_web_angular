
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/identifier.dart';

export 'actor_identifier_list.dart';

class ActorIdentifier
    extends
        Identifier<
            ActorIdentifier
            ,SampleDartWebAngularModule
        >
{
    
    ActorIdentifier() : super.implicit();

}