
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'actor_identifier.dart';

class ActorIdentifierList
    extends
        ListComputation<
            ActorIdentifier
            ,ActorIdentifierList
            ,SampleDartWebAngularModule
        >
{

    ActorIdentifierList(this.values);

    @override
    final Iterable<ActorIdentifier> values;

    @override
    ActorIdentifierList internalFactory(Iterable<ActorIdentifier> models) => ActorIdentifierList(models);

}