
import '../window_status_barrel.dart';

class WindowStatusXAxis
    extends
        WindowStatusBarrel<
            WindowStatusXAxis
        >
{

    /// 0 から 10000 までの整数（int 型）
    /// 単位は px.    
    WindowStatusXAxis(this.value);

    @override
    final int value;

    @override
    WindowStatusXAxis internalFactory(int value) => WindowStatusXAxis(value);

}