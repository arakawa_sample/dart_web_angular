
import 'package:pub_base_model/model.dart';

class ImageBackgroundStatusSize
    extends
        StringStateModel
{

    /// 原寸大。元の画像と同じ大きさ。
    ImageBackgroundStatusSize.actual()
    : value = 'auto';

    /// 縦横の比を保ち、ぴったりな大きさになる。
    /// 画像の全体像が見えないことがある。
    ImageBackgroundStatusSize.cover()
    : value = 'cover';

    /// 縦横の比を保ち、画像が全て表示される。
    /// 余白ができることがある。
    ImageBackgroundStatusSize.contain()
    : value = 'contain';

    @override
    final String value;

}