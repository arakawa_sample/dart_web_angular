
import 'package:pub_browser_dependent_model/css_property.dart';

class DotsStatusBackgroundColor
    extends
        Color
{

    const DotsStatusBackgroundColor.rgba(String value) : super.rgba(value);
    const DotsStatusBackgroundColor.black() : super.black();
    const DotsStatusBackgroundColor.blue() : super.blue();
    const DotsStatusBackgroundColor.red() : super.red();
    const DotsStatusBackgroundColor.purple() : super.purple();
    const DotsStatusBackgroundColor.green() : super.green();
    const DotsStatusBackgroundColor.yellow() : super.yellow();

}