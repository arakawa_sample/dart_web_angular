
import '../css_background_status.dart';

export 'size/dots_status_size.dart';
export 'interval/dots_status_interval.dart';
export 'dots_color/dots_status_dots_color.dart';
export 'background_color/dots_status_background_color.dart';

class DotsStatus
    extends
        CssBackgroundStatus
{

    DotsStatus(this.backgroundColor, this.dotsColor, this.size, this.interval, this.situation, [this.borderColor, this.borderWidth, this.borderStyle]);
    final DotsStatusBackgroundColor backgroundColor;
    final DotsStatusDotsColor dotsColor;
    final DotsStatusSize size;
    final DotsStatusInterval interval;
    @override
    final CssBackgroundStatusSituation situation;
    @override
    final CssBackgroundStatusBorderColor? borderColor;
    @override
    final CssBackgroundStatusBorderWidth? borderWidth;
    @override
    final CssBackgroundStatusBorderStyle? borderStyle;

    @override
    Map<String, String> toPrimitive() {
        return {
            'backgroundColor': backgroundColor.toString(),
            'dotsColor': dotsColor.toString(),
            'size': size.toString(),
            'interval': interval.toString(),
        }..addAll(super.toPrimitive());
    }

}