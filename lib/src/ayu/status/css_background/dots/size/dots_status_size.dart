
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class DotsStatusSize
    extends
        NumStateModel<num>
    with
        NumStateModelGetPx<num>
{

    /// dot の大きさ
    /// 単位は px.
    DotsStatusSize(this.value);

    @override
    final num value;

}