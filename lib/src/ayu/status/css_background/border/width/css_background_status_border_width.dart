
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class CssBackgroundStatusBorderWidth
    extends
        NumStateModel
    with
        NumStateModelGetPx
{

    CssBackgroundStatusBorderWidth(this.value);

    @override
    final num value;

}