
import 'package:pub_base_model/model.dart';

export 'mutable_css_background_status_situation.dart';

class CssBackgroundStatusSituation
    extends
        StateModel<Object>
{

    const CssBackgroundStatusSituation.initial()
    : value = 'initial';

    const CssBackgroundStatusSituation(this.value);

    @override
    final Object value;

}