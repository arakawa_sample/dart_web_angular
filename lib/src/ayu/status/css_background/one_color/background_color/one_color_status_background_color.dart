
import 'package:pub_browser_dependent_model/css_property.dart';

class OneColorStatusBackgroundColor
    extends
        Color
{

    const OneColorStatusBackgroundColor.rgba(String value) : super.rgba(value);
    const OneColorStatusBackgroundColor.black() : super.black();
    const OneColorStatusBackgroundColor.blue() : super.blue();
    const OneColorStatusBackgroundColor.red() : super.red();
    const OneColorStatusBackgroundColor.purple() : super.purple();
    const OneColorStatusBackgroundColor.green() : super.green();
    const OneColorStatusBackgroundColor.yellow() : super.yellow();

}