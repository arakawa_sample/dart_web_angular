
import 'package:pub_base_model/model.dart';
import 'package:pub_base_model/num_state_model_action.dart';

class Windows95StatusSize
    extends
        NumStateModel<num>
    with
        NumStateModelGetPx<num>
{

    Windows95StatusSize(this.value);

    @override
    final num value;

}