
import 'package:pub_browser_dependent_model/css_property.dart';

class ChecksStatusBackgroundColor
    extends
        Color
{

    const ChecksStatusBackgroundColor.rgba(String value) : super.rgba(value);
    const ChecksStatusBackgroundColor.black() : super.black();
    const ChecksStatusBackgroundColor.blue() : super.blue();
    const ChecksStatusBackgroundColor.red() : super.red();
    const ChecksStatusBackgroundColor.purple() : super.purple();
    const ChecksStatusBackgroundColor.green() : super.green();
    const ChecksStatusBackgroundColor.yellow() : super.yellow();

}