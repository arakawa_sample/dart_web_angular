
import 'package:pub_base_model/model.dart';

class SpriteBackgroundStatusRangeEndX
    extends
        IntStateModel
{

    SpriteBackgroundStatusRangeEndX(this.value);

    @override
    final int value;

}