
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';

import 'sprite_background_status.dart';

class SpriteBackgroundStatusList
    extends
        ListComputation<
            SpriteBackgroundStatus
            ,SpriteBackgroundStatusList
            ,SampleDartWebAngularModule
        >
{

    SpriteBackgroundStatusList(this.values);

    @override
    final Iterable<SpriteBackgroundStatus> values;
        
    @override
    SpriteBackgroundStatusList internalFactory(Iterable<SpriteBackgroundStatus> models) => SpriteBackgroundStatusList(models);

}
