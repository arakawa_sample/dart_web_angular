
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:sample_dart_web_angular/src/ayu/configuration/configuration_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/domain/domain_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/process/process_layer.dart';
import 'package:pub_base_model/directive.dart' as base;
import 'package:pub_browser_dependent_model/css_property.dart' as cssProperty;

import 'window_directive.template.dart' as temp;

export 'window_directive_ac.dart';

@Component(
    selector: 'ayu-window',
    templateUrl: 'window_directive.html',
    directives: [
        NgIf
        ,NgFor
        ,NgSwitch
        ,NgSwitchWhen
        ,NgSwitchDefault
        ,SpriteBackgroundProcess
        ,SpriteBackgroundProcessSwitch
        ,ImageBackgroundProcess
        ,ImageBackgroundProcessSwitch
        ,CssBackgroundProcess
        ,CssBackgroundProcessSwitch
    ],
    exports: [
    ],
    providers: [
    ]
)
class WindowDirective
    extends
        base.Directive
    implements
        OnInit
        ,AfterViewInit
        ,AfterChanges
{

    static final template = temp.WindowDirectiveNgFactory;

    static final maxWidth = 10000;
    static final maxHeight = 10000;

    WindowDirective(this.el)
    :
        /// 限界値と Duration の値を合わせることで、currentTime 1 = 1px　にできる。
        left = el.animate(
            [
                {cssProperty.Left.property: '0px'},
                {cssProperty.Left.property: '${maxWidth}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxWidth).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause() // 生成と同時に animation が動かないように

        ,bottom = el.animate(
            [
                {cssProperty.Bottom.property: '0px'},
                {cssProperty.Bottom.property: '${maxHeight}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxHeight).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause()

        ,width = el.animate(
            [
                {cssProperty.Width.property: '0px'},
                {cssProperty.Width.property: '${maxWidth}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxWidth).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause()

        ,height = el.animate(
            [
                {cssProperty.Height.property: '0px'},
                {cssProperty.Height.property: '${maxHeight}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxHeight).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause()
    ;
    
    @override
    final HtmlElement el;
    final Animation left;
    final Animation bottom;
    final Animation width;
    final Animation height;

    @Input()
    late WindowConfiguration configuration;

    @Input()
    MutableActor? linkWithActor;

    @ViewChild(SpriteBackgroundProcessSwitch)
    SpriteBackgroundProcessSwitch? spriteBackgroundProcessSwitch;

    @override
    void ngOnInit() {
        nonAnimation();
        onAnimation();
    }

    @override
    void ngAfterViewInit() {
        linkWithModel();
    }

    @override
    void ngAfterChanges() {
        linkWithSituation();
    }

    void nonAnimation() {
        el.style.display = cssProperty.Display.block().toString();
        el.style.position = cssProperty.Position.absolute().toString();

        el.style.paddingTop = configuration.windowStatus.paddingTop.px;
        el.style.paddingBottom = configuration.windowStatus.paddingBottom.px;
        el.style.paddingLeft = configuration.windowStatus.paddingLeft.px;
        el.style.paddingRight = configuration.windowStatus.paddingRight.px;
    }

    void onAnimation() {
        left.currentTime = configuration.windowStatus.x.value;
        bottom.currentTime = configuration.windowStatus.y.value;
        
        width.currentTime = configuration.windowStatus.width.value;
        height.currentTime = configuration.windowStatus.height.value;
    }

    void linkWithSituation() {
        if(spriteBackgroundProcessSwitch != null) spriteBackgroundProcessSwitch!.situation = configuration.windowStatus.spriteSituation.value;
    }

    void linkWithModel() {
        if(linkWithActor != null) {
            linkWithActor!.subscribe((event) {
                left.currentTime = event.positionX.value;
                bottom.currentTime = event.positionY.value;
                width.currentTime = event.width.value;
                height.currentTime = event.height.value;
            });
        }
    }

}