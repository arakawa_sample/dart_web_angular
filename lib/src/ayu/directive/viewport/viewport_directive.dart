
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart' hide Duration;
import 'package:sample_dart_web_angular/src/ayu/configuration/configuration_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/domain/domain_barrel.dart';
import 'package:sample_dart_web_angular/src/ayu/process/process_layer.dart';
import 'package:pub_base_model/directive.dart' as base;

import 'viewport_directive.template.dart' as temp;

@Component(
    selector: 'ayu-viewport',
    templateUrl: 'viewport_directive.html',
    directives: [
        NgIf
        ,FocusProcess
        ,NgSwitch
        ,NgSwitchWhen
        ,NgSwitchDefault
        ,SpriteBackgroundProcess
        ,SpriteBackgroundProcessSwitch
        ,ImageBackgroundProcess
        ,ImageBackgroundProcessSwitch
        ,CssBackgroundProcess
        ,CssBackgroundProcessSwitch
    ],
    exports: [
    ],
)
class ViewportDirective
    extends
        base.Directive
    implements
        OnInit
        ,AfterChanges
{

    static final template = temp.ViewportDirectiveNgFactory;

    ViewportDirective(this.el);
    final HtmlElement el;

    @ViewChild('inside')
    HtmlElement? inside;

    @Input()
    late ViewportConfiguration configuration;

    @Input()
    MutableActor? linkWithActor;

    @ViewChild(FocusProcess)
    FocusProcess? focusProcess;

   @override
    void ngOnInit() {
        nonAnimation();
    }
 
    @override
    void ngAfterChanges() {
        linkWithModel(linkWithActor, focusProcess);
    }

    void nonAnimation() {
        el.style.display = Display.block().toString();
        el.style.position = Position.relative().toString();

        /// はみ出た部分は隠れる。
        /// これによって、viewport 内にすべてのコンテンツが収まるように見える。
        el.style.overflow = Overflow.hidden().toString();

        /// scroll bar
        el.style.overflow = Overflow.scroll().toString();

        /// viewport の大きさ
        el.style.width = configuration.viewportStatus.width.px;
        el.style.height = configuration.viewportStatus.height.px;

        /// viewport の中身の大きさ
        insideProcess(inside);
    }

    void insideProcess(HtmlElement? inside) {
        if(inside == null) return;
        inside.style.display = Display.block().toString();
        inside.style.position = Position.relative().toString();
        inside.style.width = configuration.viewportStatus.insideWidth.px;
        inside.style.height = configuration.viewportStatus.insideHeight.px;
    }

    void linkWithModel(Object? model, FocusProcess? focusProcess) {
        if(model != null && model is MutableActor && focusProcess != null) {
            model.subscribe((event) {
                /// 中央に持ってくるには
                /// focuswidth / 2 - actorwidth / 2
                final offsetX = focusProcess.width.currentTime! / 2 - event.width.value / 2;
                final offsetY = focusProcess.height.currentTime! / 2 - event.height.value / 2;
                /// actor と focus を link.
                final left = event.positionX.value - offsetX;
                final bottom = event.positionY.value - offsetY;
                if(left <= 0) {
                    focusProcess.left.currentTime = 0; // 0 未満だとおかしくなる
                } else {
                    focusProcess.left.currentTime = left;
                }
                /// insideHeight - focusHeight = y の基準点
                final ybase = configuration.viewportStatus.insideHeight.value - focusProcess.height.currentTime!;
                if(bottom <= 0) {
                    focusProcess.bottom.currentTime = 0;    
                } else if(bottom >= ybase){
                    focusProcess.bottom.currentTime = ybase; // focus が画面の上の方に消えていかないように
                } else {
                    focusProcess.bottom.currentTime = event.positionY.value - offsetY;
                }
                /// focus に合わせて scroll.
                final adjustment = configuration.viewportStatus.height.value - focusProcess.height.currentTime!;
                el.scrollTo(
                    focusProcess.left.currentTime!,
                    ybase - adjustment - focusProcess.bottom.currentTime!,
                );
            });
            model.stream(); // これにより、focus の初期位置を、actor に合わせる。
        }
    }

}