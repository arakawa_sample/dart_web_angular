
import 'dart:async';
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart';
import 'package:pub_base_model/directive.dart' as base;
import 'package:sample_dart_web_angular/src/ayu/status/status_barrel.dart';

import 'image_background_process.template.dart' as temp;

export 'image_background_process_switch.dart';

@Component(
    selector: 'ayu-image-background',
    template: '',
    directives: [
    ],
    exports: [
    ],
)
class ImageBackgroundProcess
    extends
        base.Directive
    implements
        OnInit
        ,AfterViewInit
{

    static final template = temp.ImageBackgroundProcessNgFactory;

    ImageBackgroundProcess(this.el);
    final HtmlElement el;

    @Input()
    late ImageBackgroundStatus status;

    @override
    void ngOnInit() {
        el.style.position = Position.absolute().toString();
        el.style.width = '100%';
        el.style.height = '100%';
    }

    @override
    void ngAfterViewInit() {
        imageBackgroundProcess();
    }

    void imageBackgroundProcess () {

        /// image tag は使わない。css で設定する。
        el.style.backgroundImage = 'url(${status.src.value})';
        // var output = trimming(status.src.value, 500, 300).forEach((element) {
        //     print(element);
        // });
        // el.style.backgroundImage =  'url(${})';
        // trimmingg(el, status.src.value, 500, 300, 700, 200, 800, 500, 50, 25, 400, 200);
        // print(output);
        // el.style.backgroundImage = '';

        // final imageElement = ImageElement()
        //     ..src = status.src.value
        // ;

        // imageElement.onLoad.listen((event) {
        //     final canvasElement = CanvasElement(width:imageElement.naturalWidth, height: imageElement.naturalHeight);
        //     print('imageElement.naturalWidth = ${imageElement.naturalWidth}');
        //     print('imageElement.naturalHeight = ${imageElement.naturalHeight}');
        //     canvasElement.context2D
        //         .drawImage(imageElement, 0, 0)
        //     ;
        //     final afterTrimming = trimming(canvasElement, 300, 200, 1500, 800);
        //     print(afterTrimming.width);
        //     print(afterTrimming.height);
        //     final result = afterTrimming.toDataUrl('image/png');
        //     el.style.backgroundImage = 'url($result)';
        //     // el.style.backgroundImage = 'url(${status.src.value})';
        // });

        if(status.size != null) {
            el.style.backgroundSize = status.size!.value;
        } else if(
            status.width != null &&
            status.height != null
        ) {
            
            final String width;
            final String height;
            
            if (status.width!.value == 0) {
                width = 'auto';
            } else {
                width = status.width!.px;
            }

            if (status.height!.value == 0) {
                height = 'auto';
            } else {
                height = status.height!.px;
            }

            el.style.backgroundSize = '$width $height';

        }
        
        el.style.backgroundRepeatX = status.xAxisRepeat.value;
        el.style.backgroundRepeatY = status.yAxisRepeat.value;

        // el.style.backgroundPositionX = '-50px';
        // el.style.backgroundPositionY = '-50px';

        // el.style.backgroundAttachment = 'fixed';

    }

    /// timming 後、いくつかパターンがある
    ///     1.切り取った後、元の画像のサイズまでスケールさせ、大きさは、元の画像と変わらない。
    ///         縦横比が崩れることがある
    ///         切り取り時に、元の画像のサイズを超える箇所を指定し、切り取ってしまうと、その箇所が透明箇所になってしまう。
    ///     2.切り取った後、排除された箇所は、透明になり、大きさは、元の画像と変わらない。
    ///     3.切り取った後、排除された部分を取り除き、スケールもさせない。元の画像と大きさが変わる。
    CanvasElement trimming(CanvasElement source, int cutStartX, int cutStartY, int cutWidth, int cutHeight) {
        /// pattern 1
        // return CanvasElement(width:source.width, height: source.height)
        //     ..context2D.drawImageScaledFromSource(source, cutStartX, cutStartY, cutWidth, cutHeight, 0, 0, source.width!, source.height!,)
        // ;
        /// pattern 2
        // return CanvasElement(width:source.width, height: source.height)
        //     ..context2D.drawImageScaledFromSource(source, cutStartX, cutStartY, cutWidth, cutHeight, cutStartX, cutStartY, cutWidth, cutHeight)
        // ;
        /// pattern 3
        return CanvasElement(width:cutWidth, height: cutHeight)
            ..context2D.drawImageScaledFromSource(source, cutStartX, cutStartY, cutWidth, cutHeight, 0, 0, cutWidth, cutHeight,)
        ;
    }

    // HTML Canvas を使用して JavaScript で画像を切り抜く | Delft スタック: "https://www.delftstack.com/ja/howto/javascript/javascript-crop-image/"
    // 非同期なのは、ImageElement が src を読み込む処理だけ。そこだけ切り離す。他は同期処理。
    //      トリミングなどをしていくmethod で使う、drawxxには、canvas を渡す。
    //      事前準備に、一度だけ、ImageElement を canvas に渡すが、その一度だけでOK.
    StreamSubscription<Event> trimmingg(
        HtmlElement el,
        String baseImageSrc,
        int canvasWidth,
        int canvasHeight,
        int cutStartX,
        int cutStartY,
        int cutWidth,
        int cutHeight,
        int setStartX,
        int setStartY,
        int setWidth,
        int setHeight,
    ) {
        var dataURI;
        final imageElement = ImageElement()
            ..src = baseImageSrc
        ;
        final canvasElement = CanvasElement(width:canvasWidth, height: canvasHeight);
        final context2D = canvasElement.context2D;

        return imageElement.onLoad.listen((event) {
            context2D.drawImageScaledFromSource(imageElement, cutStartX, cutStartY, cutWidth, cutHeight, setStartX, setStartY, setWidth, setHeight);
            dataURI = canvasElement.toDataUrl('image/png');
            // el.style.backgroundImage = 'url($dataURI)';
            print(dataURI);
            // el.append(canvas);
            el.style.backgroundImage = 'url($dataURI)';
        });

    }

}