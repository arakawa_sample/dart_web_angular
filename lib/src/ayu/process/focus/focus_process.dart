
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:pub_browser_dependent_model/css_property.dart' hide Duration;
import 'package:pub_browser_dependent_model/css_property.dart' as cssProperty;
import 'package:sample_dart_web_angular/src/ayu/status/focus/focus_status.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/directive.dart' as base;

import 'focus_process.template.dart' as temp;

@Component(
    selector: 'ayu-focus',
    templateUrl: 'focus_process.html',
    directives: [
    ],
    exports: [
    ],
)
class FocusProcess
    extends
        base.Directive
    implements
        OnInit
        // ,AfterViewInit
{

    static final template = temp.FocusProcessNgFactory;

    static final maxWidth = 10000;
    static final maxHeight = 10000;

    FocusProcess(this.el)
    :
        left = el.animate(
            [
                {cssProperty.Left.property: '0px'},
                {cssProperty.Left.property: '${maxWidth}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxWidth).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause() // 生成と同時に animation が動かないように

        ,bottom = el.animate(
            [
                {cssProperty.Bottom.property: '0px'},
                {cssProperty.Bottom.property: '${maxHeight}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxHeight).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause()

        ,width = el.animate(
            [
                {cssProperty.Width.property: '0px'},
                {cssProperty.Width.property: '${maxWidth}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxWidth).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause()

        ,height = el.animate(
            [
                {cssProperty.Height.property: '0px'},
                {cssProperty.Height.property: '${maxHeight}px'},
            ],
            {
                cssProperty.Duration.property: cssProperty.Duration(maxHeight).value,
                cssProperty.Fill.property: cssProperty.Fill.forwards().value,
            }
        )..pause()
    ;

    final HtmlElement el;
    final Animation left;
    final Animation bottom;
    final Animation width;
    final Animation height;

    @Input()
    late FocusStatus status;

    @override
    void ngOnInit() {
        nonAnimation();
        onAnimation();
        el.style.backgroundColor = 'rgba(220,20,60,0.2)';
    }

    void nonAnimation() {
        el.style.display = Display.block().toString();
        el.style.position = Position.absolute().toString();
    }

    void onAnimation() {
        left.currentTime = status.x.value;
        bottom.currentTime = status.x.value;

        width.currentTime = status.width.value;
        height.currentTime = status.height.value;
    }

}