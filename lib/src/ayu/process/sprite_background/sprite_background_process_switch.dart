
import 'dart:html';

import 'package:angular/angular.dart';
import 'package:sample_dart_web_angular/src/ayu/status/status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_browser_dependent_model/css_property.dart' hide Duration;
import 'package:pub_base_model/directive.dart' as base;

import 'sprite_background_process.dart';

import 'sprite_background_process_switch.template.dart' as temp;

@Component(
    selector: 'ayu-sprite-background-switch',
    templateUrl: 'sprite_background_process_switch.html',
    directives: [
        NgFor
        ,NgIf
        ,NgSwitch
        ,NgSwitchWhen
        ,SpriteBackgroundProcess
    ],
    exports: [
    ],
)
class SpriteBackgroundProcessSwitch
    extends
        base.Directive
    implements
        OnInit
{

    static final template = temp.SpriteBackgroundProcessSwitchNgFactory;

    SpriteBackgroundProcessSwitch(this.el);
    final HtmlElement el;

    @ViewChild('switchContainer')
    HtmlElement? switchContainer;

    @Input()
    late SpriteBackgroundStatusList statusList;

    Object situation = 'initial';
    Map<int,List<String>> afterResultListCash = {};
    bool completedLoadCash = false;

    @override
    void ngOnInit() {
        if(switchContainer == null) return;
        switchContainer!.style.position = Position.absolute().toString();
        switchContainer!.style.top = '0';
        switchContainer!.style.bottom = '0';
        switchContainer!.style.left = '0';
        switchContainer!.style.right = '0';
        createAfterResultListCash();
    }

    /// sprite を切り替え時の見た目のちらつきを無くすには、事前に最終的な sprite(afterTrunResult) を用意しておく必要がある。
    void createAfterResultListCash() {
        /// counter と index によって、順番を固定。
        var counter = 0;
        statusList.forEach((status) {
            final imageElement = ImageElement()
                ..src = status.src.toString()
            ;
            final index = counter;
            imageElement.onLoad.listen((event) {
                afterResultListCash.addAll({
                    index: SpriteBackgroundProcess.createResult(switchContainer!, status, imageElement)
                });
                if(afterResultListCash.length == statusList.length) completedLoadCash = true;
            });
            counter++;
        });
    }

}