
import 'package:pub_base_model/iterable_action.dart';
import 'package:sample_dart_web_angular/src/ayu/status/window/window_status_barrel.dart';
import 'package:sample_dart_web_angular/src/sample_dart_web_angular_module.dart';
import 'package:pub_base_model/computation.dart';
import 'package:pub_base_model/iterable_unique_object_model.dart';
import 'package:pub_base_model/set_action.dart';

import 'mutable_window_configuration.dart';

class MutableWindowConfigurationSet
    extends
        SetComputation<
            MutableWindowConfiguration
            ,MutableWindowConfigurationSet
            ,SampleDartWebAngularModule
        >
    with
        IterableUniqueObjectModel<
            MutableWindowConfiguration
            ,MutableWindowConfigurationSet
            ,WindowStatusIdentifier
            ,WindowStatusIdentifierList
            ,SampleDartWebAngularModule
        >
        ,UniqueObjectModelSetGetById<
            MutableWindowConfiguration
            ,MutableWindowConfigurationSet
            ,WindowStatusIdentifier
            ,WindowStatusIdentifierList
            ,SampleDartWebAngularModule
        >
        ,IterableUniqueObjectModelRemoveById<
            MutableWindowConfiguration
            ,MutableWindowConfigurationSet
            ,WindowStatusIdentifier
            ,WindowStatusIdentifierList
            ,SampleDartWebAngularModule
        >
        ,UniqueObjectModelSetPut<
            MutableWindowConfiguration
            ,MutableWindowConfigurationSet
            ,WindowStatusIdentifier
            ,WindowStatusIdentifierList
            ,SampleDartWebAngularModule
        >
{

    MutableWindowConfigurationSet(this.values);

    @override
    final Iterable<MutableWindowConfiguration> values;

    @override
    MutableWindowConfigurationSet internalFactory(Iterable<MutableWindowConfiguration> models) => MutableWindowConfigurationSet(models);

    @override
    WindowStatusIdentifierList get identifiers => WindowStatusIdentifierList(values.map<WindowStatusIdentifier>((e) => e.identifier));

}